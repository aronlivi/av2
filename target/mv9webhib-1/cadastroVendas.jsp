<%@page contentType="text/html" pageEncoding="UTF-8"
        import="java.util.*"
        import="ifc.edu.br.mv9webhib.model.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Venda de Veiculo</title>
    </head>
    <body>
        <form action="Geral" method="post">
            <br>
            Vendedor 
        <select name="vendedor">
    <%
        EmEstoque emEstoque = (EmEstoque) request.getAttribute("emEstoque");
        ArrayList<Vendedor> vendedores = (ArrayList<Vendedor>) request.getAttribute("vendedores");
        for (Vendedor vendedor : vendedores) {
    %>
            <option value="<%=vendedor.getId()%>"><%=vendedor.getNome()%></option>
    <%
        }
    %>
        </select> <br>
            
            Cliente: 
        <select name="cliente">
    <%
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) request.getAttribute("clientes");
        for (Cliente cliente : clientes) {
    %>
            <option value="<%=cliente.getId()%>"><%=cliente.getNome()%></option>
    <%
        }
    %>
        </select> <br>
        Veiculo: <%= emEstoque.getCompra().getVeiculo().getModelo()%>-<%=emEstoque.getCompra().getVeiculo().getPlaca()%> <br>
        Valor Vendido: <input type="text" name="valorVendido"> <br>
            <input type="hidden" name="parent" value="venda">
            <input type="hidden" name="emEstoque" value="<%= emEstoque.getId() %>">
            <input type="submit" value="Vender">
        </form>
        <a href="index.html">Retornar ao início</a>
    </body>
</html>
