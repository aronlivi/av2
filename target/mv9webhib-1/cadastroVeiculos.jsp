<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Vendedores</title>
    </head>
    <body>
        <form action="Geral" method="post">
            Marca: <input type="text" name="marca">
            Modelo: <input type="text" name="modelo">
            Versao: <input type="text" name="versao">
            Km: <input type="text" name="km">
            Ano: <input type="text" name="ano">
            Placa: <input type="text" name="placa">
            Url Foto: <input type="text" name="urlfoto">
            Observacoes: <input type="text" name="observacoes">
            <input type="hidden" name="parent" value="veiculo">
            <input type="submit" value="Cadastrar">
        </form>
        <a href="inicial.jsp">Retornar ao início</a>
    </body>
</html>
