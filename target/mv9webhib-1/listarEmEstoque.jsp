<%@page contentType="text/html" pageEncoding="UTF-8"
        import="java.util.*"
        import="ifc.edu.br.mv9webhib.model.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Veiculos em Estoque</title>
    </head>
    <body>
        <h1>Listagem de Veiculos</h1>
        <%
            
        int listando =0;
        // goncalves2007, pg 420
        List<EmEstoque> registros = (List<EmEstoque>) request.getAttribute("registros");
        int totalRegistros = (int) request.getAttribute("totalRegistros");
        int pagina = (int) request.getAttribute("pagina");
        int tamanhoPagina = (int) request.getAttribute("tamanhoPagina");
                 
        for (EmEstoque i : registros) {
            listando++;
        %>

        <%= i.getId() %>,
        <%= i.getCompra().getVeiculo().getMarca() %>,
        <%= i.getCompra().getVeiculo().getModelo() %>,
        <%= i.getCompra().getVeiculo().getVersao() %>,
        <%= i.getCompra().getVeiculo().getAno() %>,
        <%= i.getCompra().getVeiculo().getKm() %>,
        <%= i.getCompra().getVeiculo().getUrlFoto() %>,
        <%= i.getValorVenda() %>,

        | <a href="Geral?op=vender&emEstoque=<%= i.getId() %>">Vender</a> | 
        <%
            }
        %>
<hr>
        Total de Registros: <%= totalRegistros%>
        Listando <%= listando%>
        <% if (pagina > 1) 
        {
            %>
            <a href="Geral?op=emEstoque&p=<%= pagina-1 %>&t=<%= tamanhoPagina %>">Proxima Anterior</a>
            <%
        }
        %>
        Pagina atual <%= pagina%>
        <% if (totalRegistros > (pagina * tamanhoPagina)) 
        {
            %>
            <a href="Geral?op=emEstoque&p=<%= pagina+1 %>&t=<%= tamanhoPagina %>">Proxima Pagina</a>
            <%
        }
        %>
        <hr>
        Fim da listagem
        <br>
        <a href="index.html">Retornar ao início</a>

    </body>
</html>
