<%@page contentType="text/html" pageEncoding="UTF-8"
        import="java.util.*"
        import="ifc.edu.br.mv9webhib.model.*"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>

    <link rel="stylesheet" href="css.css">
</head>

<body>
    <div class="container">
        <h2>CADASTRO</h2>
        <form action="Geral" method="post">
            <label for="nome">Nome: </label>
            <input type="text" name="nome" placeholder="Aron Livi">
            <label for="email">Email: </label>
            <input type="text" name="email" placeholder="aron@livi.com">
            <label for="salario">Salario: </label>
            <input type="text" name="salario" placeholder="R$ 1000,00">
            <label for="cargos">Cargo: </label>
            <select name="cargos">
            <%
            ArrayList<Cargo> cargos = (ArrayList<Cargo>) request.getAttribute("cargos");
            for (Cargo cargo : cargos) {
            %>
            <option id="options" value="<%=cargo.getId()%>"><%=cargo.getNome()%></option>
            <%
                   }
            %>
            </select> <br>
            <input type="hidden" name="parent" value="pessoa">
            <input id="button" type="submit" value="Cadastrar">
        </form>

        <a href="/index.html">VOLTAR</a>
    </div>
</body>

</html>