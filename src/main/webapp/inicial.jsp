<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Garage Car - Store</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="css.css">
    </head>
    <body>
        <div class="container">
            <h2>Sistema Garage Car</h2>
            <jsp:include page="subpaginaLogin.jsp"/>

            <% if (session.getAttribute("login") != null) { // tem alguém logado?
                %>
                <ul>
                    <a href="Geral?op=cadastroClientes">Cadastro de Clientes</a>
                </ul>
                <ul>
                    <a href="Geral?op=cadastroFornecedores">Cadastro de Fornecedores</a>
                </ul>
                <ul>
                    <a href="Geral?op=cadastroVendedores">Cadastro de Vendedores</a>
                </ul>
                <ul>
                    <a href="Geral?op=cadastroVeiculos">Cadastro de Veiculos</a>
                </ul>
                <ul>
                    <a href="Geral?op=cadastroCompras">Compras</a>
                </ul>
            <%  
                }
            %>
            <ul>
                <a href="Geral?op=emEstoque">Veiculos Em Estoque</a>
            </ul>

            <jsp:include page="subpaginaMensagem.jsp"/>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
        </div>
    </body>
</html>