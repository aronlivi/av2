<%@page contentType="text/html" pageEncoding="UTF-8"
        import="java.util.*"
        import="ifc.edu.br.mv9webhib.model.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compra</title>
        <link rel="stylesheet" href="css.css">
    </head>
    <body>
        <div class="container">
            <h2>Cadastrar Compra</h2>
            <form action="Geral" method="post">
                <br>
                Fornecedor: 
            <select name="fornecedor">
        <%
            ArrayList<Fornecedor> fornecedores = (ArrayList<Fornecedor>) request.getAttribute("fornecedores");
            for (Fornecedor fornecedor : fornecedores) {
        %>
                <option value="<%=fornecedor.getId()%>"><%=fornecedor.getNome()%></option>
        <%
            }
        %>
            </select> <br>
                
                Veiculo: 
            <select name="veiculo">
        <%
            ArrayList<Veiculo> veiculos = (ArrayList<Veiculo>) request.getAttribute("veiculos");
            for (Veiculo veiculo : veiculos) {
        %>
                <option value="<%=veiculo.getId()%>"><%=veiculo.getModelo()%>-<%=veiculo.getPlaca()%></option>
        <%
            }
        %>
            </select> <br>
            Valor de Compra: <input type="text" name="valor"> <br>
            Valor de Venda: <input type="text" name="venda"> <br>
                <input type="hidden" name="parent" value="compra">
                <input type="submit" value="Cadastrar">
            </form>
            <a href="index.html">Retornar ao início</a>
        </div>
    </body>
</html>
