<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Fornecedores</title>
        <link rel="stylesheet" href="css.css">
    </head>
    <body>
        <div class="container">
            <h2>Cadastrar Fornecedor</h2>
            <form action="Geral" method="post">
                Nome: <input type="text" name="nome">
                Documento: <input type="text" name="documento">
                Cidade: <input type="text" name="cidade">
                Email: <input type="text" name="email">
                <input type="hidden" name="parent" value="fornecedor">
                <input type="submit" value="Cadastrar">
            </form>
            <a href="inicial.jsp">Retornar ao início</a>
        </div>
    </body>
</html>
