/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package ifc.edu.br.mv9webhib.control;

import ifc.edu.br.mv9webhib.dao.DAO;
import ifc.edu.br.mv9webhib.model.Cargo;
import ifc.edu.br.mv9webhib.model.Cliente;
import ifc.edu.br.mv9webhib.model.Compra;
import ifc.edu.br.mv9webhib.model.EmEstoque;
import ifc.edu.br.mv9webhib.model.Fornecedor;
import ifc.edu.br.mv9webhib.model.Pessoa;
import ifc.edu.br.mv9webhib.model.Veiculo;
import ifc.edu.br.mv9webhib.model.Venda;
import ifc.edu.br.mv9webhib.model.Vendedor;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author friend
 */
@WebServlet(name = "Geral", urlPatterns = {"/Geral"})
public class Geral extends HttpServlet {
    
    private String retornarCookieLogin(HttpServletRequest request) {
        Cookie listaCookies[] = request.getCookies();
        if (listaCookies != null) {
            for (Cookie c : listaCookies) {
                if (c.getName().equals("mv9webhib-1-login")) {
                    return c.getValue();
                }
            }
        }
        return null;
    }
    
    DAO dao = new DAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession sessao = request.getSession(true);
        String op = request.getParameter("op");
        if (op == null) {
            if (sessao.getAttribute("login") == null) { // está desconectado?
                String loginNoCookie = retornarCookieLogin(request);
                if (loginNoCookie != null) {
                    // só faz essa ação 1 vez: tem no cookie, mas não tem na sessão
                    // problema: verifica toda vez se tem atributo na sessão
                    // workaround: usar op=inicio nos direcionamentos, 
                    // em vez de apenas chamar o servlet Geral (op=null)
                    // adiciona login na sessao
                    sessao.setAttribute("login", loginNoCookie);
                    sessao.setAttribute("mensagem", "Bem vindo novamente!");
                }
            }
            getServletContext().getRequestDispatcher("/inicial.jsp").forward(request, response);
        } else if (op.equals("inicio")) {
            getServletContext().getRequestDispatcher("/inicial.jsp").forward(request, response);
        } else if (op.equals("formlogin")) {
            getServletContext().getRequestDispatcher("/formlogin.jsp").forward(request, response);
        } else if (op.equals("logout")) {
            sessao.removeAttribute("login"); // remove login da sessão

            // remove o cookie
            Cookie ckLogin = new Cookie("mv9webhib-1-login", "");
            ckLogin.setMaxAge(0);
            response.addCookie(ckLogin);

            // usa redirect para, após o clique, 
            // não permanece na barra de endereços: Geral?op=logout
            response.sendRedirect(request.getContextPath() + "/Geral");
            //getServletContext().getRequestDispatcher("/formlogin.jsp").forward(request, response);
        } else if (op.equals("cadastroPessoas")) {
            getServletContext().getRequestDispatcher("/cadastroPessoas.jsp").forward(request, response);
        } else if (op.equals("cadastroVendedores")) {
            getServletContext().getRequestDispatcher("/cadastroVendedores.jsp").forward(request, response);
        } else if (op.equals("cadastroFornecedores")) {
            getServletContext().getRequestDispatcher("/cadastroFornecedores.jsp").forward(request, response);
        } else if (op.equals("cadastroClientes")) {
            getServletContext().getRequestDispatcher("/cadastroClientes.jsp").forward(request, response);
        } else if (op.equals("cadastroVeiculos")) {
            getServletContext().getRequestDispatcher("/cadastroVeiculos.jsp").forward(request, response);
        } else if (op.equals("cadastroCompras")) {
            request.setAttribute("veiculos", dao.consultarVeiculo());
            request.setAttribute("fornecedores", dao.consultarFornecedor());
            getServletContext().getRequestDispatcher("/cadastroCompras.jsp").forward(request, response);
        } else if (op.equals("cadastroVendas")) {
            getServletContext().getRequestDispatcher("/cadastroVendas.jsp").forward(request, response);
            
        } else if (op.equals("emEstoque"))
        {
            int totalRegistros = dao.countEmEstoque(); 
            request.setAttribute("totalRegistros", totalRegistros);    
            // obtém dados
            
            int pagina =  validaInt(request.getParameter("p"));
            if (pagina == 0) pagina = 1;
            int tamanhoPagina =  validaInt(request.getParameter("t"));
            if (tamanhoPagina == 0) tamanhoPagina = 10;
            
            List<EmEstoque> registros = dao.retornarEmEstoque(tamanhoPagina,pagina);
            // insere no request
            request.setAttribute("registros", registros);
            
            request.setAttribute("pagina", pagina);
            
            request.setAttribute("tamanhoPagina", tamanhoPagina);
            
            getServletContext().getRequestDispatcher("/listarEmEstoque.jsp").forward(request, response);
        }
        else if (op.equals("vender"))
        {
            
            EmEstoque emEstoque = dao.consultarEmEstoque(validaLong(request.getParameter("emEstoque")));
            request.setAttribute("emEstoque", emEstoque);
            request.setAttribute("clientes", dao.consultarCliente());
            request.setAttribute("vendedores", dao.consultarVendedor());

           
            getServletContext().getRequestDispatcher("/cadastroVendas.jsp").forward(request, response);
        }
        
        else {
            getServletContext().getRequestDispatcher("/inicial.jsp").forward(request, response);
        }
        
        
        
        /*processRequest(request, response);
        if (request.getParameter("visualizar") != null) {
            request.setAttribute("pessoas", dao.consultarPessoas());
            getServletContext().getRequestDispatcher("/listar.jsp").forward(request, response);
        } else if (request.getParameter("cadastroPessoa") != null) {
            request.setAttribute("cargos", dao.consultarCargos());
            getServletContext().getRequestDispatcher("/cadastro.jsp").forward(request, response);
        } else if (request.getParameter("cadastroCargo") != null) {
            getServletContext().getRequestDispatcher("/cadastroCargo.jsp").forward(request, response);
        }*/
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               HttpSession sessao = request.getSession(true);

        String op = request.getParameter("op");
        if (op == null) {
            getServletContext().getRequestDispatcher("/inicial.jsp").forward(request, response);
        } else if (op.equals("login")) {
            String login = request.getParameter("login");
            String senha = request.getParameter("senha");
            if (login.equals("admin") && senha.equals("123")) {
                sessao.setAttribute("login", login);
                sessao.setAttribute("mensagem", "Login efetuado com sucesso");
                sessao.setAttribute("id", Long.parseLong("1"));

                // guarda o login em cookie, para permanecer logado se fechar o browser
                Cookie ckLogin = new Cookie("mvCookieLogin-1-login", login);
                ckLogin.setMaxAge(24 * 60 * 60); // permanece por 1 dia
                //ckLogin.setPath("/");
                response.addCookie(ckLogin);

                // usa redirect para, após o clique, 
                // não permanece na barra de endereços: Geral?op=logout
                response.sendRedirect(request.getContextPath() + "/Geral");
                return;
            } else {
                sessao.setAttribute("mensagem", "Erro no login e/ou senha, tente novamente.");
                getServletContext().getRequestDispatcher("/formlogin.jsp").forward(request, response);
            }
        } 
        if ("cliente".equals(request.getParameter("parent"))) {
            Cliente c = new Cliente();
            c.setNome(request.getParameter("nome"));
            c.setEmail(request.getParameter("email"));
            c.setCidade(request.getParameter("cidade"));
            c.setDocumento(request.getParameter("documento"));
            c.setData_criacao(new Date());
            c.setUsuario_criacao_id((Long)sessao.getAttribute("id"));

            //c.setCargo(dao.consultarCargo(validaLong(request.getParameter("cargos"))));
            dao.inserirCliente(c);
        } else if ("fornecedor".equals(request.getParameter("parent"))) {
            Fornecedor f = new Fornecedor();
            f.setNome(request.getParameter("nome"));
            f.setEmail(request.getParameter("email"));
            f.setCidade(request.getParameter("cidade"));
            f.setDocumento(request.getParameter("documento"));
            f.setData_criacao(new Date());
            
            f.setUsuario_criacao_id((Long)sessao.getAttribute("id"));

            dao.inserirFornecedor(f);   
        }else if ("vendedor".equals(request.getParameter("parent"))) {
            Vendedor v = new Vendedor();
            v.setNome(request.getParameter("nome"));
            v.setEmail(request.getParameter("email"));
            v.setCidade(request.getParameter("cidade"));
            v.setDocumento(request.getParameter("documento"));
            v.setComissao(validaDouble(request.getParameter("comissao")));
            v.setData_criacao(new Date());
            
            v.setUsuario_criacao_id((Long)sessao.getAttribute("id"));

            dao.inserirVendedor(v);   
        }else if ("veiculo".equals(request.getParameter("parent"))) {
            Veiculo v = new Veiculo();
            v.setModelo(request.getParameter("modelo"));
            v.setMarca(request.getParameter("marca"));
            v.setVersao(request.getParameter("versao"));
            v.setAno(validaDouble(request.getParameter("ano")));
            v.setKm(validaDouble(request.getParameter("km")));
            v.setPlaca(request.getParameter("placa"));
            v.setObservacao(request.getParameter("observacao"));
            v.setUrlFoto(request.getParameter("urlfoto"));
            v.setData_criacao(new Date());
            
            v.setUsuario_criacao_id((Long)sessao.getAttribute("id"));

            dao.inserirVeiculo(v);   
        }
        else if ("compra".equals(request.getParameter("parent"))) {
            Compra c = new Compra();
            Fornecedor f = dao.consultarFornecedor(validaLong(request.getParameter("fornecedor")));
            Veiculo v = dao.consultarVeiculo(validaLong(request.getParameter("veiculo")));
            c.setVeiculo(v);
            c.setFornecedor(f);
            c.setValorCompra(validaFloat(request.getParameter("valorCompra")));
            c.setData_criacao(new Date());
            c.setUsuario_criacao_id((Long)sessao.getAttribute("id"));

 
            EmEstoque e = new EmEstoque();
            e.setCompra(c);
            e.setValorVenda(validaFloat(request.getParameter("valorCompra")));
            e.setData_criacao(new Date());
            e.setUsuario_criacao_id((Long)sessao.getAttribute("id"));

            dao.processarCompra(c,e);   
        }
        else if ("venda".equals(request.getParameter("parent"))) {
            Venda v = new Venda();
            EmEstoque e = dao.consultarEmEstoque(validaLong(request.getParameter("emEstoque")));
            Cliente c = dao.consultarCliente(validaLong(request.getParameter("cliente")));
            Vendedor vd = dao.consultarVendedor(validaLong(request.getParameter("vendedor")));
            float valorComissao = 0f;
            float valorVendido = validaFloat(request.getParameter("valorVendido"));
            if (vd != null && vd.getComissao() >0)
            {
                float percentualComissao = (float)vd.getComissao();
                valorComissao = (valorVendido *percentualComissao ) / 100.0f;
            }
            
            v.setVeiculo(e.getVeiculo());
            v.setCliente(c);
            v.setEmEstoque(e);
            v.setValorComissao(valorComissao);
            v.setValorVendido(valorVendido);
            v.setVendedor(vd);
            v.setData_criacao(new Date());
            v.setUsuario_criacao_id((Long)sessao.getAttribute("id"));
            v.setData_alteracao(new Date());
            v.setUsuario_alteracao_id((Long)sessao.getAttribute("id"));
            e.setData_alteracao(new Date());
            e.setUsuario_alteracao_id((Long)sessao.getAttribute("id"));

            dao.processarVenda(v,e);   
        }
        else {
            response.sendRedirect(request.getContextPath() + "/Geral");
        }
        
        /*processRequest(request, response);
        request.setAttribute("msg", "Cadastro realizado com sucesso!");
        getServletContext().getRequestDispatcher("/mensagem.jsp").forward(request, response);*/
    }
    
    private Float validaFloat(String s) {
        try {
            return Float.parseFloat(s);
        } catch(Exception e) {
            return 0F;
        }
    }
    
    private Double validaDouble(String s) {
        try {
            return Double.parseDouble(s);
        } catch(Exception e) {
            return 0D;
        }
    }
    
    private int validaInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch(Exception e) {
            return 0;
        }
    }
    
    private Long validaLong(String s) {
        try {
            return Long.parseLong(s);
        } catch(Exception e) {
            return 0L;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
