package ifc.edu.br.mv9webhib.dao;

import ifc.edu.br.mv9webhib.model.Cargo;
import ifc.edu.br.mv9webhib.model.Cliente;
import ifc.edu.br.mv9webhib.model.Compra;
import ifc.edu.br.mv9webhib.model.EmEstoque;
import ifc.edu.br.mv9webhib.model.Fornecedor;
import ifc.edu.br.mv9webhib.model.Pessoa;
import ifc.edu.br.mv9webhib.model.Usuario;
import ifc.edu.br.mv9webhib.model.Veiculo;
import ifc.edu.br.mv9webhib.model.Venda;
import ifc.edu.br.mv9webhib.model.Vendedor;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAO {
    
    EntityManagerFactory emf;
    EntityManager em;

    public DAO() {
        emf = Persistence.createEntityManagerFactory("meuPU");
        em = emf.createEntityManager();
    }
    
    public void inserirPessoa(Pessoa p) {
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.persist(p);
        tx.commit();
    }
    
    
    public List consultarPessoas() {
        List pessoas = em.createQuery("from Pessoa", Pessoa.class).getResultList();
        return pessoas;
    }
   
    public void inserirUsuario(Usuario u) {
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.persist(u);
        tx.commit();
    }
    
    public List consultarUsuarios() {
        List usuarios = em.createQuery("from Usuarios", Usuario.class).getResultList();
        return usuarios;
    }
    
    public Usuario consultarUsuarioComSenha(String usuario, String senha) {
        Query q = em.createQuery("from Cargo where usuario = :usuario and senha = :senha", Usuario.class);
        q.setParameter("usuario", usuario);
        q.setParameter("senha", senha);
        return (Usuario) q.getSingleResult();
    }
    
    public void inserirCargo(Cargo c) {
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.persist(c);
        tx.commit();
    }
    
    public List consultarCargos() {
        List cargos = em.createQuery("from Cargo", Cargo.class).getResultList();
        return cargos;
    }
    
    public Cargo consultarCargo(Long id) {
        Query q = em.createQuery("from Cargo where id = :id", Cargo.class);
        q.setParameter("id", id);
        return (Cargo) q.getSingleResult();
    }
    
    
    
    public void inserirCliente(Cliente c) {
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.persist(c);
        tx.commit();
    }
    
    public List consultarCliente() {
        List cargos = em.createQuery("from Cliente", Cliente.class).getResultList();
        return cargos;
    }
    
    public Cliente consultarCliente(Long id) {
        Query q = em.createQuery("from Cliente where id = :id", Cliente.class);
        q.setParameter("id", id);
        return (Cliente) q.getSingleResult();
    }
    
    public void inserirFornecedor(Fornecedor f) {
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.persist(f);
        tx.commit();
    }
    
    public List consultarFornecedor() {
        List cargos = em.createQuery("from Fornecedor", Fornecedor.class).getResultList();
        return cargos;
    }
    
    public Fornecedor consultarFornecedor(Long id) {
        Query q = em.createQuery("from Fornecedor where id = :id", Fornecedor.class);
        q.setParameter("id", id);
        return (Fornecedor) q.getSingleResult();
    }
    
    public void inserirVendedor(Vendedor v) {
        EntityTransaction tx = em.getTransaction();
        
        tx.begin();
        em.persist(v);
        tx.commit();
    }
    
    public List consultarVendedor() {
        List cargos = em.createQuery("from Vendedor where Inativo = 0", Vendedor.class).getResultList();
        return cargos;
    }
    
    public Vendedor consultarVendedor(Long id) {
        Query q = em.createQuery("from Vendedor where id = :id", Vendedor.class);
        q.setParameter("id", id);
        return (Vendedor) q.getSingleResult();
    }
    
    
    public void inserirVeiculo(Veiculo v) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(v);
        tx.commit();
    }
    
    public List consultarVeiculo() {
        List cargos = em.createQuery("from Veiculo where Inativo = 0", Veiculo.class).getResultList();
        return cargos;
    }
    
    public Veiculo consultarVeiculo(Long id) {
        Query q = em.createQuery("from Veiculo where id = :id", Veiculo.class);
        q.setParameter("id", id);
        return (Veiculo) q.getSingleResult();
    }
    
    public Compra consultarCompra(Long id) {
        Query q = em.createQuery("from Compra where id = :id", Compra.class);
        q.setParameter("id", id);
        return (Compra) q.getSingleResult();
    }
    
    public void processarCompra(Compra compra, EmEstoque emEstoque) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(compra);
        emEstoque.setCompra(compra);
        em.persist(emEstoque);
        tx.commit();
    }
    
    public EmEstoque consultarEmEstoque(Long id) {
        Query q = em.createQuery("from EmEstoque where id = :id", EmEstoque.class);
        q.setParameter("id", id);
        return (EmEstoque) q.getSingleResult();
    }
    public int countEmEstoque() {
        Query query = em.createQuery("SELECT COUNT(p) FROM EmEstoque p where p.inativo = 0 " );
        return query.getSingleResult() != null ? Integer.parseInt(query.getSingleResult().toString()) : 0;
    }
    
    public List<EmEstoque> retornarEmEstoque(int tamanhoPagina, int pagina) {
        //int registroPagina = 1;
        
        //if (pagina > 1) registroPagina = (pagina-1) * tamanhoPagina;
        
        Query selectQuery = em.createQuery("From EmEstoque where inativo = 0");
        selectQuery.setFirstResult((pagina - 1) * tamanhoPagina);
        selectQuery.setMaxResults(tamanhoPagina);
        return selectQuery.getResultList();
    }
    
    public void processarVenda(Venda venda, EmEstoque emEstoque) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(venda);
        emEstoque.setInativo(true);
        em.persist(emEstoque);
        tx.commit();
    }
}
