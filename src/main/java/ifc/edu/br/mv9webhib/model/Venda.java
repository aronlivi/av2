/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ifc.edu.br.mv9webhib.model;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "ortiz_av1c_venda")
@SequenceGenerator(name = "default_gen", sequenceName = "venda_seq", allocationSize = 1, initialValue = 1)
public class Venda extends EntidadeBase {
    
    @ManyToOne()
    Veiculo veiculo;

    @ManyToOne()
    EmEstoque emEstoque;

    @ManyToOne()
    Vendedor vendedor;

    @ManyToOne()
    Cliente cliente;

    
    private Float valorVendido;
    private Float valorComissao;

    public Venda() {
    }


    public Float getValorVendido() {
        return valorVendido;
    }

    public void setValorVendido(Float valorVendido) {
        this.valorVendido = valorVendido;
    }
    
    public Float getValorComissao() {
        return valorComissao;
    }

    public void setValorComissao(Float valorComissao) {
        this.valorComissao = valorComissao;
    }

    @Override
    public String toString() {
        return "Venda";
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }
    public EmEstoque getEmEstoque() {
        return emEstoque;
    }

    public void setEmEstoque(EmEstoque emEstoque) {
        this.emEstoque = emEstoque;
    }
    
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

}
