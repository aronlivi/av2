/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ifc.edu.br.mv9webhib.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "ortiz_av1c_fornecedor")
@SequenceGenerator(name = "default_gen", sequenceName = "fornecedor_seq", allocationSize = 1, initialValue = 1)
public class Fornecedor extends PessoaBase {
    
    public Fornecedor(String nome) {
        super(nome, "", "", "");
    }

    public Fornecedor() {
        super("", "", "", "");
    }    

}
