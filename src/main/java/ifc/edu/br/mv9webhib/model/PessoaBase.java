/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifc.edu.br.mv9webhib.model;

import jakarta.persistence.MappedSuperclass;

/**
 *
 * @author ortiz
 */
@MappedSuperclass
public class PessoaBase extends EntidadeBase{

    
    private String nome;
    private String email;
    private String documento;
    private String cidade;
    
    public PessoaBase(String nome, String email, String documento, String cidade)
    {
        this.nome = nome;
        this.email = email;
        this.documento = documento;
        this.cidade = cidade;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
   
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }


    
}
