/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ifc.edu.br.mv9webhib.model;

import jakarta.persistence.Entity;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table(name = "ortiz_av1c_veiculo")
@SequenceGenerator(name = "default_gen", sequenceName = "veiculo_seq", allocationSize = 1, initialValue = 1)
public class Veiculo extends EntidadeBase {

    private double km;
    private double ano;
    private String marca;
    private String modelo;
    private String versao;
    private String placa;
    private String observacao;
    private String urlfoto;
 
    public Veiculo(String marca, String modelo, String versao, String placa) {
        
    }

    public Veiculo() {
    }    
    
    public double getKm() {
        return km;
    }

    public void setKm(double km) {
        this.km = km;
    }
    
    public double getAno() {
        return ano;
    }

    public void setAno(double ano) {
        this.ano = ano;
    }
    
    
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getVersao() {
        return versao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    public String getUrlFoto() {
        return urlfoto;
    }

    public void setUrlFoto(String urlfoto) {
        this.urlfoto = urlfoto;
    }
}
