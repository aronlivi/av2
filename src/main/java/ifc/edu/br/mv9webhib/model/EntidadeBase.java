/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifc.edu.br.mv9webhib.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import java.util.Date;

/**
 *
 * @author ortiz
 */
@MappedSuperclass
public class EntidadeBase {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen")
    private Long id= 0L;
    
    private Date data_criacao;
    private Long usuario_criacao_id;
    private Date data_alteracao;
    private Long usuario_alteracao_id;
    private boolean inativo;
    
    public EntidadeBase()
    {
    }
    

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData_criacao() {
        return data_criacao;
    }

    public void setData_criacao(Date data_criacao) {
        this.data_criacao = data_criacao;
    }

    public Long getUsuario_criacao_id() {
        return usuario_criacao_id;
    }

    public void setUsuario_criacao_id(Long usuario_criacao_id) {
        this.usuario_criacao_id = usuario_criacao_id;
    }

    public Date getData_alteracao() {
        return data_alteracao;
    }

    public void setData_alteracao(Date data_alteracao) {
        this.data_alteracao = data_alteracao;
    }

    public Long getUsuario_alteracao_id() {
        return usuario_alteracao_id;
    }

    public void setUsuario_alteracao_id(Long usuario_alteracao_id) {
        this.usuario_alteracao_id = usuario_alteracao_id;
    }

    
    public boolean getInativo() {
        return inativo;
    }

    public void setInativo(boolean inativo) {
        this.inativo = inativo;
    }
    
}
